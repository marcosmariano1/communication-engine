package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/marcelocpinheiro/communication-engine/management_api/config"
)

func main() {
	//Instância um novo roteador MUX, um roteador http para go
	r := mux.NewRouter()

	// Repassa as rotas para o roteador
	http.Handle("/", r)

	// Cria um handler para a url "/ping", escreve "running" na tela e retorna o status 200
	r.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "Running")
		w.WriteHeader(http.StatusOK)
	})

	// Cria um logger para a aplicação
	logger := log.New(os.Stderr, "logger: ", log.Lshortfile)

	// Instancia um novo servidor com configurações específicas de timeout, endereço, log e handler
	srv := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		Addr:         ":" + strconv.Itoa(config.API_PORT),
		Handler:      context.ClearHandler(http.DefaultServeMux),
		ErrorLog:     logger,
	}

	//Printa na tela a porta que a aplicação está ouvindo
	log.Printf("Listening on port %d", config.API_PORT)

	//inicia o servidor e, se houver erro, mata a aplicação e printa o erro
	err := srv.ListenAndServe()
	if err != nil {
		log.Fatal(err.Error())
	}
}
