package config

const (
	DB_USER     = "communication_engine"
	DB_PASSWORD = "communication_engine"
	DB_DATABASE = "communication_engine"
	DB_HOST     = "127.0.0.1"
	API_PORT    = 8080
)
